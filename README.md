# Computational Metaphysics

Formal logic (and formal reasoning) constitutes an substantial role in philosophy, mathematics and computer science. Topics such as basic reasoning in propositional and (first-order) predicate logic are often dealt with in early lecture of the aforementioned study programs. Unfortunately, other core topics are often omitted. This lecture addresses different advanced logic systems that are employed within modern studies in (theoretical) philosophy, mathematics and computer science. In conjunction therewith, a practical introduction to computer-assisted reasoning is given and their application to contemporary problems is demonstrated.

In the first part of the lecture we will retrace different variants of the Ontological Argument using computer deduction systems. Subsequently, we investigate the theory of abstract objects (by E.N. Zalta) as a logical foundation of metaphysics as well as means of representation in a computer system. Finally we discuss the interdisciplinary impact of these results and their influence on the young research area of computational metaphysics.

In student projects the learned techniques will be applied to similar arguments of metaphysics. Every promising project can be extended (with further supervision, if necessary) towards a publication in the field of logics or logical philosophy.

## Lecture web site
http://www.inf.fu-berlin.de/users/lex/lehre/compmeta/